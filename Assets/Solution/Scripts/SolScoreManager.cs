using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SolScoreManager : MonoBehaviour {
    [SerializeField]
    private int currentScore = 0;
    public TMP_Text scoreText;

    public void Start(){
        UpdateScoreText();
    }

    public int GetScore(){
        return currentScore;
    }

    public void SetScore(int value){
        currentScore = value;
        UpdateScoreText();
    }

    public void AddScore(int value){
        currentScore += value;
        UpdateScoreText();
    }

    public void UpdateScoreText(){
        scoreText.text = "Current Score: " + currentScore;
    }
}
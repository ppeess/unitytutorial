using UnityEngine;

public class SolPickUp : MonoBehaviour {
    [SerializeField]
    private int value = 10;

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player"){
            GameObject.Find("ScoreManager").GetComponent<SolScoreManager>().AddScore(value);
            Destroy(gameObject);
        }
    }
}
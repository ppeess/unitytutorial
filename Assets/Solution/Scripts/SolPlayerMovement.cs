using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class SolPlayerMovement : MonoBehaviour
{
    private CharacterController characterController;
    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float sprintModifier = 0.3f;
    private bool isSprinting = false;
    private float turnSmoothVelocity;
    [SerializeField]
    private float turnTime = 0.1f;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 movement = new Vector3(horizontal, 0f, vertical).normalized;

        if(Input.GetKey(KeyCode.LeftShift)){
            isSprinting = true;
        }
        else {
            isSprinting = false;
        }

        if(movement.magnitude > 0f){
            float targetAngle = Mathf.Atan2(movement.x, movement.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnTime);
            transform.rotation = Quaternion.Euler(0, angle, 0);
            characterController.Move(movement * (speed + (Convert.ToInt32(isSprinting) * sprintModifier * speed)) * Time.deltaTime);
        }
    }
}
 